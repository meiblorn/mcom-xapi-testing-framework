package com.macys.xapi.test.framework.core.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "test", ignoreUnknownFields = true)
class TestProperties {

    var environments = emptyList<Environment>()
    var endpoints = emptyList<Endpoint>()

    class Environment {
        lateinit var name: String
        lateinit var url: String
    }

    class Endpoint {
        lateinit var name: String
        lateinit var template: String
    }
}
