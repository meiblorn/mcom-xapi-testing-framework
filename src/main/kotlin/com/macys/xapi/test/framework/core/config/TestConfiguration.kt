package com.macys.xapi.test.framework.core.config

import com.macys.xapi.test.framework.core.config.properties.TestProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(TestProperties::class)
class TestConfiguration {
}
