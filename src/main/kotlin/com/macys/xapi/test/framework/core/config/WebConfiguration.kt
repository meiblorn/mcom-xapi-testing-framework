package com.macys.xapi.test.framework.core.config

import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate


@Configuration
class WebConfiguration {

    @Bean
    fun httpClient(): CloseableHttpClient = HttpClientBuilder.create().build()

    @Bean
    fun requestFactory() = HttpComponentsClientHttpRequestFactory(httpClient());

    @Bean
    fun restTemplate(restTemplateBuilder: RestTemplateBuilder): RestTemplate =
            restTemplateBuilder
                    .requestFactory { requestFactory() }
                    .build()


}
