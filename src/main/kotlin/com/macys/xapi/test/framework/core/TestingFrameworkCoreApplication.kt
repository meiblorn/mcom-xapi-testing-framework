package com.macys.xapi.test.framework.core

import com.macys.xapi.test.framework.core.config.properties.TestProperties
import com.macys.xapi.test.framework.core.config.properties.TestProperties.Environment
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils.replace
import org.springframework.web.client.RestTemplate

@SpringBootApplication
class TestingFrameworkCoreApplication {

    @Service
    class Runner : CommandLineRunner {

        @Autowired
        private lateinit var logger: Logger

        @Autowired
        private lateinit var testProperties: TestProperties

        @Autowired
        private lateinit var restTemplate: RestTemplate;

        override fun run(vararg args: String?) {
            val endpoints = testProperties.endpoints
            val environments = testProperties.environments

            for (endpoint in endpoints) {
                val responses = mutableMapOf<Environment, Any?>()

                for (environment in environments) {
                    val url = replace(endpoint.template, "{{host_url}}", environment.url)
                    val response = try {
                        restTemplate.exchange(url, HttpMethod.GET, null, String::class.java)
                    } catch (e: Exception) {
                        logger.warn("Exception when calling $url endpoint", e)
                        null
                    }

                    responses[environment] = response;
                }

//                for (responseEntry in responses.entries) {
//                    for (environment in environments) {
//
//                    }
//                }
            }

        }

    }

}

fun main(args: Array<String>) {

    runApplication<TestingFrameworkCoreApplication>(*args) {
        webApplicationType = WebApplicationType.NONE
    }

}
